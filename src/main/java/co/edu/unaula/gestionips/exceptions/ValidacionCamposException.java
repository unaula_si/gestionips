package co.edu.unaula.gestionips.exceptions;

public class ValidacionCamposException extends RuntimeException {

    public enum Type {
        NO_ES_ESTRUCTURA_IP("Dato no es valida.");

        private final String message;

        public ValidacionCamposException build() {
            return new ValidacionCamposException(this);
        }

        Type(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

    }

    private final Type type;

    private ValidacionCamposException(Type type) {
        super(type.message);
        this.type = type;
    }

    public Type getType() {
        return type;
    }

}
