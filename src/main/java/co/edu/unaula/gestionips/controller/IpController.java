package co.edu.unaula.gestionips.controller;

import co.edu.unaula.gestionips.controller.base.ControladorBase;
import co.edu.unaula.gestionips.converter.IpConverter;
import co.edu.unaula.gestionips.dto.IpDTO;
import co.edu.unaula.gestionips.service.IpServiceTor;
import co.edu.unaula.gestionips.service.IpServiceTorBulkexit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping(value = "api/")
public class IpController extends ControladorBase {
    private final IpServiceTorBulkexit ipService;
    private final IpServiceTor ipServiceTor;

    @Autowired
    public IpController(IpServiceTorBulkexit ipService, IpServiceTor ipServiceTor) {
        this.ipService = ipService;
        this.ipServiceTor = ipServiceTor;
    }

    @GetMapping(value = "ips")
    public Mono<ResponseEntity<List<IpDTO>>> obtenerIps() {
        return ipService.obtenerIpsTor()
                .concatWith(ipServiceTor.obtenerIpsTor())
                .distinct()
                .map(IpConverter::convertirModelADto)
                .collectList()
                .map(this::validarLista);
    }
}