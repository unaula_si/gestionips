package co.edu.unaula.gestionips.repository;

import co.edu.unaula.gestionips.documents.IpDocument;
import co.edu.unaula.gestionips.models.Ip;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface IpRepository extends ReactiveMongoRepository<IpDocument, String> {
}
