package co.edu.unaula.gestionips.service;

import co.edu.unaula.gestionips.converter.IpConverter;
import co.edu.unaula.gestionips.models.Ip;
import co.edu.unaula.gestionips.repository.IpRepository;
import co.edu.unaula.gestionips.utils.StringValidators;
import co.edu.unaula.gestionips.utils.TransformadorIps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;

import java.net.URI;

@Service
@Slf4j
public class IpServiceTorBulkexit {
    private final IpRepository ipRepository;
    private final WebClient webClient;
    private final StringValidators stringValidators;

    @Value("${application.urlTorBulkexit}")
    private String urlThor;

    private static final String CONTENT_TYPE_VALUE = "application/json";
    private static final String CONTENT_TYPE = "Content-Type";

    @Autowired
    public IpServiceTorBulkexit(IpRepository ipRepository, WebClient webClient, StringValidators stringValidators) {
        this.ipRepository = ipRepository;
        this.webClient = webClient;
        this.stringValidators = stringValidators;
    }

    public Flux<Ip> obtenerIpsTor() {
        return webClient
                .get()
                .uri(crearURI())
                .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMapMany(clientResponse ->
                        clientResponse.bodyToMono(String.class))
                .flatMap(TransformadorIps::sacarIps)
                .flatMap(stringValidators::validarEsIp)
                .distinct()
                .map(IpConverter::convertirModelADocument)
                .collectList()
                .flatMapMany(ips ->
                        ipRepository.saveAll(ips)
                                .map(IpConverter::convertirDocumentAModel))
                .onErrorResume(throwable -> obtenerIpsTorDb());
    }

    private Flux<Ip> obtenerIpsTorDb(){
        return ipRepository.findAll()
                .map(IpConverter::convertirDocumentAModel);
    }

    private URI crearURI() {
        return UriComponentsBuilder
                .fromUriString(urlThor)
                .build()
                .toUri();
    }
}