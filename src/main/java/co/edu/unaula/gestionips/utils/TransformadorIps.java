package co.edu.unaula.gestionips.utils;

import co.edu.unaula.gestionips.models.Ip;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.stream.Collectors;

public class TransformadorIps {
    public static Flux<Ip> sacarIps(String ips) {
        return Flux.fromIterable(Arrays.stream(ips.split("\n")).collect(Collectors.toList()))
                .map(stringIp ->
                        Ip.builder()
                                .ip(stringIp)
                                .build());
    }
}
