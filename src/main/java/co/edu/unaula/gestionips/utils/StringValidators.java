package co.edu.unaula.gestionips.utils;

import co.edu.unaula.gestionips.exceptions.ValidacionCamposException;
import co.edu.unaula.gestionips.models.Ip;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class StringValidators {
    @Value("${application.expresionIps}")
    private String expresionSoloIps;

    public Mono<Ip> validarEsIp(Ip ip) {
        Pattern estructuraIp = Pattern.compile(expresionSoloIps);
        List<String> a = new ArrayList<>();
        a.add(ip.getIp());
        boolean tieneValoresConCaracteresEspeciales = !a
                .stream()
                .filter(valor ->
                        !estructuraIp.matcher(valor).matches())
                .collect(Collectors.toList())
                .isEmpty();
        if (tieneValoresConCaracteresEspeciales) {
            return Mono.error(ValidacionCamposException.Type.NO_ES_ESTRUCTURA_IP.build());
        }
        return Mono.just(ip);
    }
}
