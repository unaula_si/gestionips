package co.edu.unaula.gestionips.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class IpDTO {
    private final String ip;
}