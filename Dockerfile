FROM openjdk:8-jdk-alpine
VOLUME [ "/home" ]
EXPOSE 8087
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=release", "/home/gestionips-0.0.1-SNAPSHOT.jar"]